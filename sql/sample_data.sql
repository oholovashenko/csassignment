INSERT INTO readings.room (id, name)
VALUES
    (1, 'First'), 
    (2, 'Second');

INSERT INTO readings.sensor (id, type, room_id)
VALUES
    (1, 'U', 1),
    (2, 'R', 1),
    (3, 'R', 1),
    (4, 'U', 2),
    (5, 'U', 2),
    (6, 'R', 2),
    (7, 'R', 2),
    (8, 'R', 2);

TRUNCATE TABLE readings.reading;
-- smoke test data --

-- TODO: arrange as a unit test.
-- TODO: performance comparison between solutions on a dataset of millions rows generated with mutating CTE.
-- TODO: -- Custom aggregate function solution with Coalesce
         -- Views

-- Assuming max of 100 rooms now --

-- cases covered:
--  1. in first room, time 10:00:02 - missing U
--  2. in second room, time 10:00:01 - missing R - must not take value from other room. Should result in no data(last entry, no previous results)
--  3. in first room, there are mixing value and NULL value R readings. Check corectness 
--  4. multiple consecutive NULL R entries. Which discards LAG-solution
--  5. in first room, there are no closest discrete for R, last available value is NULL at 09:59:56

-- all above cases covered in second_task.sql


INSERT INTO readings.reading (time, value, sensor_id)
VALUES
    ('2020-06-30T10:00:02.800001', NULL, 1),
    ('2020-06-30T10:00:02.800001', 55.2, 4),
    ('2020-06-30T10:00:02.800001', 56.1, 5),
    ('2020-06-30T10:00:02.500002', 20.5, 2),
    ('2020-06-30T10:00:02.500002', 25.5, 6),
    ('2020-06-30T10:00:02.000002', 26.1, 7),
    ('2020-06-30T10:00:02.000001', 27.1, 8),
    ('2020-06-30T10:00:02.000002', 20.1, 2),
    ('2020-06-30T10:00:02.000001', 21.1, 3),
    
    ('2020-06-30T10:00:01.800001', 50.1, 1),
    ('2020-06-30T10:00:01.800001', 55.2, 4),
    ('2020-06-30T10:00:01.800001', 56.1, 5),
    ('2020-06-30T10:00:01.500002', 20.5, 2),
    ('2020-06-30T10:00:01.500002', NULL, 6),
    ('2020-06-30T10:00:01.000002', NULL, 7),
    ('2020-06-30T10:00:01.000001', NULL, 8),
    ('2020-06-30T10:00:01.000002', 20.1, 2),
    ('2020-06-30T10:00:01.000001', 21.1, 3),

    ('2020-06-30T10:00:00.800001', 50.0, 1),
    ('2020-06-30T10:00:00.500001', NULL, 2),
    ('2020-06-30T10:00:00.000002', NULL, 2),
    ('2020-06-30T10:00:00.000001', 21.5, 3),
    ('2020-06-30T10:00:00.500002', NULL, 6),
    ('2020-06-30T10:00:00.000002', NULL, 7),
    ('2020-06-30T10:00:00.000001', NULL, 8),

    ('2020-06-30T09:59:59.800001', 50.1, 1),
    ('2020-06-30T09:59:59.500001', NULL, 2),
    ('2020-06-30T09:59:59.000002', NULL, 2),
    ('2020-06-30T09:59:59.000001', NULL, 3),

    ('2020-06-30T09:59:58.800001', 50.2, 1),
    ('2020-06-30T09:59:58.500001', NULL, 2),
    ('2020-06-30T09:59:58.000002', NULL, 2),
    ('2020-06-30T09:59:58.000001', NULL, 3),
    
    ('2020-06-30T09:59:57.800001', 50.3, 1),
    ('2020-06-30T09:59:57.500001', 22.2, 2),
    ('2020-06-30T09:59:57.000002', NULL, 2),
    ('2020-06-30T09:59:57.000001', NULL, 3),
    
    ('2020-06-30T09:59:56.800001', 50.4, 1),
    ('2020-06-30T09:59:56.500001', NULL, 2),
    ('2020-06-30T09:59:56.000002', NULL, 2),
    ('2020-06-30T09:59:56.000001', NULL, 3);