-- CTE + Window function solution

WITH grouped AS (
    SELECT
        sensor.room_id room_id,
        date_trunc('second', time) time_truncated,
        TRUNC(avg(value),2) value_average,
        type
    FROM
        readings.reading 
    INNER JOIN
        readings.sensor as sensor
        ON sensor.id = reading.sensor_id
    GROUP BY
        time_truncated,
        room_id,
        type
    ORDER BY
        time_truncated DESC
),
    partitioned AS (
    SELECT
        groupU.room_id room_id,
        groupU.time_truncated time_truncated,
        groupU.value_average voltage,
        groupR.value_average resistance,
        -- here we are assuming the longest series after join would be 1000000.
        --  next step: count it above
        groupU.room_id*1000000 + sum(case
                when groupR.value_average is null 
                then 0 
                else 1
            end) over (partition by groupU.room_id order by groupR.time_truncated asc) as resistance_partition
    FROM
        grouped groupU
    LEFT JOIN 
        grouped groupR
        ON
            groupU.time_truncated=groupR.time_truncated
        AND
            groupU.room_id=groupR.room_id
        AND
            groupR.type='R'
    WHERE
        groupU.type='U' 
        AND
        groupU.value_average IS NOT NULL
),
    extended AS (
    SELECT
        room_id,
        time_truncated,
        voltage,
        resistance,
        -- partition by room_id may be enough here, need more tests --
        first_value(resistance) over (partition by room_id, resistance_partition order by time_truncated asc) resistance_extended,
        resistance_partition
    FROM
        partitioned
)
SELECT
    room_id Помещение,
    time_truncated Время,
    TRUNC(voltage / resistance_extended, 2) I,
    voltage U,
    resistance_extended R
FROM
    extended
WHERE
    -- resistance_extended is NULL only if this entry's resistance is NULL 
    --      and there are no not-NULL entries before for this particular room
    resistance_extended IS NOT NULL
ORDER BY
    time_truncated desc;
