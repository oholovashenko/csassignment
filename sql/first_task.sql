WITH grouped AS (
    SELECT
        sensor.room_id room_id,
        date_trunc('second', time) time_truncated,
        TRUNC(avg(value),2) value_average,
        type
    FROM
        readings.reading 
    INNER JOIN
        readings.sensor as sensor
        ON sensor.id = reading.sensor_id
    GROUP BY
        time_truncated,
        room_id,
        type
    ORDER BY
        time_truncated DESC
)
SELECT 
    groupU.room_id Помещение,
    groupU.time_truncated Время,
    TRUNC(groupU.value_average / groupR.value_average, 2) I,
    groupU.value_average U,
    groupR.value_average R
FROM
    grouped groupU
LEFT JOIN 
    grouped groupR
    ON
        groupU.time_truncated=groupR.time_truncated
    AND
        groupU.room_id=groupR.room_id
    AND
        groupR.type='R'
WHERE
    groupU.type='U' 
    AND
    groupU.value_average IS NOT NULL;
