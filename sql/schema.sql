-- CREATE DATABASE csassignment
--     WITH 
--     OWNER = test
--     ENCODING = 'UTF8'
--     CONNECTION LIMIT = -1;


-- CREATE USER test WITH ENCRYPTED PASSWORD 'test';


DROP SCHEMA IF EXISTS readings CASCADE;
CREATE SCHEMA IF NOT EXISTS readings AUTHORIZATION test;


CREATE TABLE readings.room
(
    id integer NOT NULL,
    name character varying(128) COLLATE pg_catalog."default",
    CONSTRAINT room_pkey PRIMARY KEY (id),
    CONSTRAINT room_name_key UNIQUE (name)
)

TABLESPACE pg_default;

ALTER TABLE readings.room
    OWNER to test;

CREATE TYPE readings.sensor_type AS ENUM
    ('U', 'R');

ALTER TYPE readings.sensor_type
    OWNER TO test;

CREATE TABLE readings.sensor
(
    id integer NOT NULL,
    name character varying(128) COLLATE pg_catalog."default",
    type sensor_type NOT NULL,
    room_id integer NOT NULL,
    CONSTRAINT sensor_pkey PRIMARY KEY (id),
    FOREIGN KEY (room_id) REFERENCES readings.room (id)
)

TABLESPACE pg_default;

ALTER TABLE readings.sensor
    OWNER to test;

CREATE TABLE readings.reading
(
    id SERIAL NOT NULL,
    "time" timestamp NOT NULL
        DEFAULT current_timestamp,
    value numeric(3,1),
    sensor_id integer,
    CONSTRAINT reading_pkey PRIMARY KEY (id),
    FOREIGN KEY (sensor_id) REFERENCES readings.sensor (id)
)

-- TODO: add index on not null --

TABLESPACE pg_default;

ALTER TABLE readings.reading
    OWNER to test;