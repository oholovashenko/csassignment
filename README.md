# Run

Clone this repository:

```git clone --recursive https://oholovashenko@bitbucket.org/oholovashenko/csassignment.git```


# Golang

golang/ directory contains some demos prepared with help of Docker-compose:

```make starve```

```make overflow```

And at last

```make work```

Sure, components could be run without any containers. Entrypoints are located in cmd/ subdirectories.

Implementation does not pretend to be full and covered with tests. It is more of a demonstration
